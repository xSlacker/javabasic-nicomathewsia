

package javabasic.nico_mathew_sia;
import java.math.BigInteger;


public class TestMyTime{

    public static void main(String[] args)
    {
     
        MyTime mt = new MyTime(12,30,22);
        System.out.println(mt.toString());
        mt.setTime(22,30,22);
        System.out.println(mt.toString());
        
        System.out.println(mt.getHour());
        System.out.println(mt.getMinute());
        System.out.println(mt.getSecond());
        mt.setHour(11);
        mt.setMinute(11);
        mt.setSecond(11);
        System.out.println(mt.toString());
        
        System.out.println(mt.nextHour());
                
        System.out.println(mt.nextMinute());
                
        System.out.println(mt.nextSecond());
        
        
        System.out.println(mt.previousHour());
                
        System.out.println(mt.previousMinute());
                
        System.out.println(mt.previousSecond());
  
    }
    
    
}
    
    
