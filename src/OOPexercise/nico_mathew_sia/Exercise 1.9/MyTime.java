
package javabasic.nico_mathew_sia;

public class MyTime {

    private int hour = 0;
    private int minute = 0;
    private int second = 0;
    
    public MyTime(int hour, int minute, int second)
    {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    
    public void setTime(int hour, int minute, int second)
    {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        
    }
    
    public int getHour()
    {
        return this.hour;
    }
    
    
    public int getMinute()
    {
        return this.minute;
    }
    
    
    public int getSecond()
    {
        return this.second;
    }
    
    public void setHour(int hour)
    {
        this.hour = hour;
    }
    
    public void setMinute(int minute)
    {
        this.minute = minute;
    }
    
    
    public void setSecond(int second)
    {
        this.second = second;
    }
    
    
    public String toString()
    {
        return ""+this.hour+":"+this.minute+":"+this.second;
    }
    
    public MyTime nextSecond()
    {
        if(this.second == 59)
        {
            this.second = 00;
            if(this.minute==59)
            {
                this.minute =00;
                if(this.hour==23)
                {
                    this.hour = 01;
                }
            }
        }
        else
            this.second++;
        
        return this;
    }
    
    
    public MyTime nextMinute()
    {
        if(this.minute==59)
        {
            this.minute =00;
            if(this.hour==23)
            {
                this.hour = 01;
            }
        }
        else
            this.minute++;
        
        return this;
    }
    
    
    public MyTime nextHour()
    {
        if(this.hour==23)
        {
                this.hour = 01;
        }
        else
            this.hour++;
        
        return this;
    }
    
    
    public MyTime previousSecond()
    {
        if(this.second == 00)
        {
            this.second = 59;
            if(this.minute==00)
            {
                this.minute =59;
                if(this.hour==00)
                {
                    this.hour = 23;
                }
                else
                    this.hour--;
            }
            else
                this.minute--;
                
        }
        else
            this.second--;
        
        return this;
    }
    
    
    public MyTime previousMinute()
    {
        
            if(this.minute==00)
            {
                this.minute =59;
                if(this.hour==00)
                {
                    this.hour = 23;
                }
                else
                    this.hour--;
            }
            else
                this.minute--;
         
            return this;
    }
    
    
    public MyTime previousHour()
    {
        
                if(this.hour==00)
                {
                    this.hour = 23;
                }
                else
                    this.hour--;

                    return this;
    
    }
        
        
}
