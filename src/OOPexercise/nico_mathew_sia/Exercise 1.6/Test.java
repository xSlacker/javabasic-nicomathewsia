

package javabasic.nico_mathew_sia;
import java.util.Scanner;

import java.util.Date;


public class Test {

        
    public static void main(String[] args)
    {
     
        double real;
        double imag;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter complex number 1 (real and imaginary part): ");
        real = in.nextDouble();
        imag = in.nextDouble();
        MyComplex mc1 = new MyComplex(real,imag);
        MyComplex mc3 = new MyComplex(real,imag);
        
        System.out.print("Enter complex number 2 (real and imaginary part): ");
        real = in.nextDouble();
        imag = in.nextDouble();
        
        MyComplex mc2 = new MyComplex(real,imag);
        
        
        System.out.println("\n"+"Number 1 is: "+mc1);
        if(mc1.isReal())
            System.out.println(mc1+" is a pure real number");
        else
            System.out.println(mc1+" is NOT a pure real number");
        
        if(mc1.isImaginary())
            System.out.println(mc1+" is a pure imaginary number");
        else
            System.out.println(mc1+" is NOT a pure imaginary number");
            
        
        System.out.println("\n"+"Number 2 is: "+mc2);
        if(mc2.isReal())
            System.out.println(mc2+" is a pure real number");
        else
            System.out.println(mc2+" is NOT a pure real number");
        
        if(mc2.isImaginary())
            System.out.println(mc2+" is a pure imaginary number");
        else
            System.out.println(mc2+" is NOT a pure imaginary number");
        
            
        if(mc1.equals(mc2))
            System.out.println("\n"+mc1+" is equal to"+mc1);
        else
            System.out.println("\n"+mc1+" is NOT equal to"+mc1);
        
        
        System.out.print(mc1+" + "+mc2+" = ");
        System.out.println(mc1.add(mc2));
        
        
        System.out.print(mc3+" - "+mc2+" = ");
        System.out.println(mc3.subtract(mc2));
        
        
        
    }
    
    
    
    
}
    
    
