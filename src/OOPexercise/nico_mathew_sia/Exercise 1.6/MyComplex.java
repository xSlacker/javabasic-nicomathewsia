
package javabasic.nico_mathew_sia;

public class MyComplex {

    private double real;
    private double imag;
    
    public MyComplex(double real, double imag)
    {
        this.real =  real;
        this.imag = imag;
    }
    
    public double getReal()
    {
        return this.real;
    }
    
    public void setReal(double real)
    {
        this.real = real;
    }
    
    public double getImag()
    {
        return this.imag;
    }
    
    public void setImag(double imag)
    {
        this.imag = imag;
    }
    
    public void setValue(double real, double imag)
    {
        this.imag = imag;
        this.real = real;
    }
    
    public String toString()
    {
        return "("+real+" + "+imag+"i)";
    }
    
    public boolean isReal()
    {
        return (imag==0);
    }
    
    public boolean isImaginary()
    {
        return (imag==0);
    }
    
    public boolean equals(double real,double imag)
    {
        if(this.real == real && this.imag == imag)
            return true;
        return false;
    }
    
    public  boolean equals(MyComplex another)
    {
        if(this.real == another.getReal() && this.imag == another.getImag())
            return true;
        return false;
    }
    
    public double magnitude()
    {
        return Math.sqrt(real+imag);
    }
    
    public double argumentInRadians()
    {
        return (double)Math.atan2(imag, real);
    }
    
    public int argumentInDegrees()
    {
        return (int)Math.atan2(imag, real);
    }
    
    public MyComplex conjugate()
    {
        return new MyComplex(real,-imag);
    }
    
    public MyComplex add(MyComplex another)
    {
        this.imag += another.imag;
        this.real += another.real;
        
        return this;
    }
    
    public MyComplex subtract(MyComplex another)
    {
        
        this.imag -= another.imag;
        this.real -= another.real;
        
        return this;
    }
    
    
    public MyComplex multiplyWith(MyComplex another)
    {
        this.imag *= another.imag;
        this.real *= another.real;
        
        return this;
    }
    
    
    public MyComplex dividBy(MyComplex another)
    {
        this.imag /= another.imag;
        this.real /= another.real;
        
        return this;
    }
    
        
    
    
}

