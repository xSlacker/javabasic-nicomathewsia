
package javabasic.nico_mathew_sia;


public class Circle {

    private double radius;
    private String color;

    public Circle()
    {
          radius = 1.0;
          color = "red";
    }

    public Circle(double radius)
    {
        
        color = "red";
        this.radius = radius;
    }

    public double getRadius()
    {
        return this.radius;
    }


    public double getArea()
    {
        return Math.PI * (this.radius*this.radius);
    }

    public String toString()
    {
        return "Circle with Radius:"+this.radius+" Color:"+this.color;
    }


}
