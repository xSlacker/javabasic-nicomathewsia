package javabasic.nico_mathew_sia;

public class Cylinder{
    
    private Circle base;
    private double height;
    
    public Cylinder()
    {
        base = new Circle();
        this.height = 1.0;
    }
    
    public Cylinder(double height)
    {
        base = new Circle();
        this.height = height;
    }
    
    public Cylinder(double radius,double height)
    {
        base = new Circle(radius);
        this.height = height;
    }
    
    
    public double getHeight()
    {
        return height;
    }
    
    public double getVolume()
    {
        return base.getArea()*height;
    }
    
    
    public double getRadius()
    {
        return base.getRadius();
    }
    
    
    public double getArea()
    {
        return (2*Math.PI*this.height) + (2*( Math.PI * (base.getRadius() * base.getRadius())));
    }
    
    public String toString()
    {
        return "Cylinder with base of "+base.toString()+" and height of"+this.height;
    }
            
}
