

package javabasic.nico_mathew_sia;

public class Ball {
    
    private float x;
    private float y;
    private int radius;
    private float xDelta;
    private float yDelta;
    
    
    public Ball(int x, int y, int radius, int speed,int direction)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;

        this.xDelta = (float)(speed * Math.cos(direction));
        this.yDelta = (float)((speed*-1) * Math.sin(direction));
    }
    
    
    public void setXY(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public void setX(int x)
    {
        this.x = x;
    }
    
    public float getX()
    {
        return this.x;
    }
    
    public void setY(int y)
    {
        this.y = y;
    }
    
    public float getY()
    {
        return this.y;
    }
    
    public void setRadius(int redius)
    {
        this.radius = radius;
    }
    
    
    public int getRadius()
    {
        return this.radius;
    }
    
    public void setYDelta(int yDelta)
    {
        this.yDelta = yDelta;
    }
    
    public float getYDelta()
    {
        return this.yDelta;
    }
    
    
    public void setXDelta(int xDelta)
    {
        this.xDelta = xDelta;
    }
    
    
    public float getXDelta()
    {
        return this.xDelta;
    }
    
    public void move()
    {
        this.x += this.xDelta;
        this.y += this.yDelta;
    }
    
    public void reflectHorizontal()
    {
        xDelta =  xDelta * -1;
    }
    
    public void reflectVertical()
    {
        
        yDelta =  yDelta * -1;
    }
    
    public String toString()
    {
        return "Ball at ("+this.x+","+this.y+")"+" of velocity ("+this.xDelta+","+this.yDelta+").";
    }
}
