
package javabasic.nico_mathew_sia;

public class Container {
 
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    
    public Container(int x, int y, int width, int height)
    {
        this.x1 = x;
        this.y1 = y;
        this.x2 = x1 + width-1;
        this.y2 = y1 - height-1;
    }
    
    public boolean collidesWith(Ball ball)
    {
        if(ball.getX() - ball.getRadius() <= this.x1 ||
           ball.getX() - ball.getRadius() >= this.x2)
        {
            ball.reflectHorizontal();
            return true;
        }
        
        if(ball.getY() - ball.getRadius() <= this.y1 ||
           ball.getY() - ball.getRadius() >= this.y2)
        {
            ball.reflectVertical();
            return true;
        }
        
        return false;
    }
    
    public void setX(int x, int width)
    {
        this.x1 = x1;
        this.x2 = x1 + width-1;
    }
    
    
    
    public void setY(int y ,  int height)
    {
        this.y1 = y1;
        this.y2 = y1 - height-1;
    }
    
    
    
    public int getX()
    {
        return this.x1;
    }
    
    
    public int getY()
    {
        return this.y1;
    }
    
   
    
    public String toString()
    {
        
        return null;
    }
    
    
}
