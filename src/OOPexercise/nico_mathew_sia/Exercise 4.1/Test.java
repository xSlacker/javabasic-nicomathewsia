

package javabasic.nico_mathew_sia;


public class Test{

    public static void main(String[] args)
    {
      
       Shape s1 = new Circle(5.5,"RED", false);
       System.out.println(s1);
       System.out.println(s1.getArea());
       System.out.println(s1.getPerimeter());
       System.out.println(s1.getColor());
       System.out.println(s1.isFilled());
       //System.out.println(s1.getRadius()); getRadius is not declared on the shape class.   
        
        Circle c1 = (Circle)s1;
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());
        
        //Shape s2 = new Shape(); abstract classess cannot be instantiated 
        
        Shape s3 = new Rectangle(1.0 , 2.0, "RED", false);
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        //System.out.println(s3.getLength()); getLength is not declared on the shape class.
        
        Rectangle r1 = (Rectangle)s3;
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());
        
        
        //Shape s4 = new Square(6,6); over parameter Shape class constructor accepts 1 double or 1 double,1 string and 1 boolean
        Shape s4 = new Square(6);
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        //System.out.println(s4.getSide());  getSide method is not declared on Shape class 
        
        
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        //System.out.println(r2.getSide()); getSide method is not declared on Rectangle class 
        System.out.println(r2.getLength());
        
        
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
        
    }
    
    
    
}
    
    
