package javabasic.nico_mathew_sia;

import java.util.Calendar;

public class MyDate {
    
    private int year;
    private int month;
    private int day;
    
    private String[] strMonths = {"Jan","Feb","Mar","Apr","May","Jun",
                                  "Jul","Aug","Sep","Oct","Nov","Dec"};
    private String[] strDays = {"Sunday","Monday","Tuesday","Wednesday",
                                "Thursday","Friday","Saturday"};
    
    private int[] daysInMonths = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    public boolean isLeapYear(int year)
    {
        if(year%4==0)
            return true;
        
        return false;
    }
    
    public boolean isValidDate(int year,int month,int day)
    {
        if(isLeapYear(year))
        {
            if(month == 2)
            {
                if(day>29 || day<1)
                    return false;
            }
            else
            {
                if(year<1 || month<1 || day<1 || month>12 || day > daysInMonths[month-1])
                    return false;
            }
        }
        else if(year<1 || month<1 || day<1 || month>12 || day > daysInMonths[month-1])
                return false;
        
        return true;
    }
    
    public int getDayOfWeek(int year,int month,int day)
    {
        
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, day);

        return cal.get(Calendar.DAY_OF_WEEK);
    }
    
    public MyDate(int year,int month,int day)
    {
        if(isValidDate(year,month,day))
        {
            this.day = day;
            this.year = year;
            this.month = month;
        }
        else
            System.out.println("Invalid Date!");
    }
    
    public void setDate(int year,int month,int day)
    {
        
        this.day = day;
        this.year = year;
        this.month = month;
    }
    
    public int getYear()
    {
        return this.year;
    }
    
    public int getMonth()
    {
        return this.month;
    }
    
    public int getDay()
    {
        return this.day;
    }
    
    public void setYear(int year)
    {
        
        if(isValidDate(year,this.month,this.day))
        {
            this.year = year;
        }
        else
            System.out.println("Invalid Year!");
        
    }
    
    public void setMonth(int month)
    {
        if(isValidDate(this.year,month,this.day))
        {
            this.month = month;
        }
        else
            System.out.println("Invalid Month!");
    }
    
    public void setDay(int day)
    {
        
        if(isValidDate(this.year,this.month,day))
        {
            this.day = day;
        }
        else
            System.out.println("Invalid day!");
    }
    
    public String toString()
    {
        return ""+strDays[getDayOfWeek(this.year,this.month,this.day)-1]+" "+this.day+" "+strMonths[this.month-1]+" "+this.year;
    }
    
    public MyDate nextDay()
    {
        
        
        if(((isLeapYear(this.year)) && (this.month == 02) && (this.day==29)) )
        {
            
            this.day = 01;
            this.month++ ;
        }
        else if( (this.day == daysInMonths[this.month-1]) && !((this.month == 02) && (this.day==28)) )
        {
            this.day = 01;
            
            if(this.month == 12)
            {
                this.month = 01;
                this.year++;
            }
            else
                this.month++ ;
            
        }
        else 
            this.day++;
        
        return this;
    }
    
    public MyDate nextMonth()
    {
        
            if(this.month == 12)
            {
                this.month = 01;
                this.year++;
            }
            else
            {
                this.month++ ;
                
                if(this.day > daysInMonths[this.month-1])
                {
                    this.day = daysInMonths[this.month-1];
                }
            }
        return this;
    }
    
    public MyDate nextYear()
    {
        
        this.year++;
        
        return this;
    }
    
    
    public MyDate previousDay()
    {
        
        if( this.day == 01 )
        {
            
            if(this.month == 01)
            {
                this.day = 31;         
                this.month = 12;
                this.year--;
                
            }
            else if( isLeapYear(this.year) && this.month == 03)
            {
                    this.day = daysInMonths[this.month-2];         
                    this.month--;
                    this.day = 29;
            }
            else 
            {
                this.day = daysInMonths[this.month-2];
                this.month-- ;
            }
            
        }
        else
            this.day--;
        
        return this;
    }
    
    
    public MyDate previousMonth()
    {
        
            if(this.month == 01)
            {
                this.month = 12;
                this.year--;
                
                if(this.day > daysInMonths[this.month-1])
                {
                    this.day = daysInMonths[this.month-1];
                }
            }
            else if( isLeapYear(this.year) && this.month == 03)
            {
                    this.month--;
                    this.day = 29;
            }
            else 
            {
                this.month-- ;
                if(this.day > daysInMonths[this.month-1])
                {
                    this.day = daysInMonths[this.month-1];
                }
            }
            
        
        return this;
    }
    
    
    
    public MyDate previousYear()
    {
        if( isLeapYear(this.year) && this.month == 02)
        {
            if(this.day > daysInMonths[this.month-1])
            {
                this.day = daysInMonths[this.month-1];
            }
        }
        
        this.year--;
        
        return this;
    }
    
    
    
    
}
