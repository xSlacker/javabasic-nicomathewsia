

package javabasic.nico_mathew_sia;

public class Circle {
    
    private double radius;
    private String color;
    
    public Circle()
    {
        this.radius = 1.0;
        this.color = "red";
    }
    
    public Circle(double r)
    {
        this.radius = r;
        this.color = "red";
    }

    //added 3rd constructor
    public Circle(double r,String c)
    {
        this.radius = r;
        this.color = c;
    }
    
    
    //added 3rd color getter
    public String getColor()
    {
        return color;
    }
    
    public double getRadius()
    {
        return radius;
    }
    
    public double getArea()
    {
        return radius*radius*Math.PI;
    }
    
    //added radius setter
    public void setRadius(double r)
    {
        this.radius = r;
    }
    
    //added color setter
    public void setColor(String c)
    {
        this.color=c;
    }
    
    public String toString()
    {
        return "Circle: radius=" +radius + " color="+color;
    }
        
}
