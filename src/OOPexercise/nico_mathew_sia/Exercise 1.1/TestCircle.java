

package javabasic.nico_mathew_sia;
import java.util.Scanner;


public class TestCircle {

    public static void main(String[] args)
    {
        
        Circle c1 = new Circle();
        System.out.println("The circle has radius of "
            +c1.getRadius() + " and area of "+ c1.getArea());
   
        System.out.println(c1.toString());
        
        Circle c2 = new Circle(2.0);
        System.out.println("The circle has radius of "
            +c2.getRadius() + " and area of "+ c2.getArea());

        Circle c3 = new Circle(2.0,"Blue");
        
        System.out.println("The circle has radius of "
            +c3.getRadius() + " and area of "+ c3.getArea()+ " and color of "+ c3.getColor());

        c3.setColor("red");
        c3.setRadius(8);
        System.out.println("The circle has radius of "
            +c3.getRadius() + " and area of "+ c3.getArea()+ " and color of "+ c3.getColor());

    }
    
    
        
    
}
