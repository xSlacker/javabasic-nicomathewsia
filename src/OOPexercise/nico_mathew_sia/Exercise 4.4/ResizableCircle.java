package javabasic.nico_mathew_sia;

public class ResizableCircle extends Circle implements Resizable{

    public ResizableCircle(double radius)
    {
        super(radius);
    }
    
    @Override
    public double resize(int percent)
    {
        super.radius = (double)percent/100 * (double)super.radius;
        return super.radius;
    }
    
    
}
