package javabasic.nico_mathew_sia;

public interface Resizable {
    public double resize(int percent);
}
