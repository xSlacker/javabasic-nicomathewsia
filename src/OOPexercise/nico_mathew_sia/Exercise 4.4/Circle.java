
package javabasic.nico_mathew_sia;


public class Circle implements GeometricObject {

    protected double radius;

    public Circle()
    {
        radius = 1.0;
    }

    public Circle(double radius)
    {
        
        this.radius = radius;
    }
    
    @Override
    public double getArea()
    {
        return Math.PI * (this.radius*this.radius);
    }
    
    @Override
    public double getPerimeter()
    {
        return 2*Math.PI *this.radius;
    }


    
}
