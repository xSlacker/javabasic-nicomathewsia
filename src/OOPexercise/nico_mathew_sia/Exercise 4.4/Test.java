

package javabasic.nico_mathew_sia;

import java.util.Date;


public class Test {

        
    public static void main(String[] args)
    {
        
    
        ResizableCircle rc = new ResizableCircle(10);
        System.out.println("Radius : "+rc.radius);
        System.out.println("Area : "+rc.getArea());
        System.out.println("Perimeter : "+rc.getPerimeter());
        
        rc.resize(90);
        System.out.println("Radius : "+rc.radius);
        System.out.println("Area : "+rc.getArea());
        System.out.println("Perimeter : "+rc.getPerimeter());
        
    }
    
    
    
}
    
    
