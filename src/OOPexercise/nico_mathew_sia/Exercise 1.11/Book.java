

package javabasic.nico_mathew_sia;

public class Book {

    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name,Author[] authors,double price)
    {
        this.name = name;
        this.authors = authors;
        this.price = price;

    }
    public Book(String name,Author[] authors,double price,int qtyInStock)
    {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;

    }

    public String getName()
    {
        return this.name;
    }

    public Author[] getAuthor()
    {
        return this.authors;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getQtyInstock()
    {
        return this.qtyInStock;
    }

    public void setQtyInstock()
    {
        this.qtyInStock = qtyInStock;
    }

    public String toString()
    {
        String authors = "";
        if(this.authors.length>0)
        {
            for(int x=0 ; x<this.authors.length-1;x++)
            {
                authors +=this.authors[x].getName()+",";
            }

                authors +=this.authors[this.authors.length-1].getName();

        }

        return this.name+" by "+authors;
    }

    public void printAuthors()
    {
        if(this.authors.length>0)
        {
            for(int x=0 ; x<this.authors.length-1;x++)
            {
                System.out.print(this.authors[x].getName()+",");
            }

            System.out.print(this.authors[this.authors.length-1].getName());

        }

    }





}
