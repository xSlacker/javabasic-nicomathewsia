

package javabasic.nico_mathew_sia;
import java.math.BigInteger;


public class TestBook{

    public static void main(String[] args)
    {
     
       Author[] authors = new Author[2];
       authors[0] = new Author("test1","email1",'m');
       authors[1] = new Author("test2","email2",'m');
       
       Book javaDummy = new Book("Java for Dummy", authors,19.99 , 99);
       System.out.println(javaDummy);
       System.out.print("The authors are: ");
       javaDummy.printAuthors();
    }
    
    
    
}
    
    
