package javabasic.nico_mathew_sia;
import java.util.Scanner;
import java.io.*;


public class MyPolynomial {
    
    
    private double[] coeffs;
    
    public MyPolynomial(double... coeffs)
    {
        this.coeffs =  coeffs;
    }
    public MyPolynomial(String filename)
    {
        Scanner in = null;
        try
        {
            in = new Scanner(new File(filename));
            
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        
        int degree = in.nextInt();
        coeffs = new double[degree+1];
        for(int i=0; i<coeffs.length ; i++)
        {
            coeffs[i] = in.nextDouble();
        }
        
    }
    
    public int getDegree()
    {
        int d = coeffs.length-1;
            while((coeffs[d] == 0) && (d > 0))
        d--;
            
        return d;
    }
    
    public String toString()
    {
        return null;
    }
    
    public double evaluate(double x)
    {    
        double sum = 0;
        double input = 1;
        for(int k=0; k<coeffs.length; k++) 
        {
          sum += coeffs[k]*input;
          input *= x;
        }
        return sum;
    }
    
    public MyPolynomial add(MyPolynomial another)
    {
        int degree = getDegree();
        if(another.getDegree() > degree)
        {
            degree = another.getDegree();
        }
        double[] coefficients = new double[degree];
        for(int i=0; i<getDegree(); i++)
            coefficients[i] += coeffs[i];
         for(int i=0; i<another.getDegree(); i++)
            coefficients[i] += another.coeffs[i];
        
         
         return new MyPolynomial(coefficients);
    }
    
    public MyPolynomial multiply(MyPolynomial another)
    { 
        int degree = getDegree();
        if(another.getDegree() > degree)
        {
            degree = another.getDegree();
        }
        double[] coefficients = new double[degree];
        for(int i=0; i<getDegree(); i++)
            coefficients[i] *= coeffs[i];
         for(int i=0; i<another.getDegree(); i++)
            coefficients[i] *= another.coeffs[i];
        
         
         return new MyPolynomial(coefficients);
    }
    
    
}
