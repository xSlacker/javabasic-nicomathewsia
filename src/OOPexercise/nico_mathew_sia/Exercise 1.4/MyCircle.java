package javabasic.nico_mathew_sia;


public class MyCircle {
    
    private MyPoint center;
    private int radius = 1;
    
    public MyCircle(int x, int y, int radius)
    {
        this.center = new MyPoint();
        this.center.setXY(x, y);
        this.radius = radius;
    }
    public MyCircle(MyPoint center,int radius)
    {
        
        this.center = new MyPoint();
        this.center = center;
        this.radius = radius;
    }
    
    public int getRadius()
    {
        return this.radius;
    }
    
    public void setRadius(int radius)
    {
        this.radius =  radius;
    }
    
    public void setCenter(MyPoint center)
    {
        this.center = center; 
    }
    
    public int getCenterX()
    {
        return this.center.getX();
    }
    
    
    public int getCenterY()
    {
        return this.center.getY();
    }
    
    
    public void setCenterXY(int x, int y)
    {
        this.center.setXY(x, y);
    }
    
    public String toString()
    {
        return "center:"+this.center+" radius:"+radius;
    }
    
    public double getArea()
    {
        return Math.PI *(radius*radius);
    }
    
}
