

package javabasic.nico_mathew_sia;


public class TestMyCircle{

    public static void main(String[] args)
    {

        MyCircle mc = new MyCircle(2,2,4);
        System.out.println(mc);
        System.out.println("radius: "+mc.getRadius());
        mc.setRadius(5);
        System.out.println("radius: "+mc.getRadius());
        System.out.println("getCenterX: "+mc.getCenterX());
        System.out.println("getCenterY: "+mc.getCenterY());
        mc.setCenterXY(3, 3);
        System.out.println("getCenterX: "+mc.getCenterX());
        System.out.println("getCenterY: "+mc.getCenterY());
        System.out.println("Area: "+mc.getArea());
        
        MyCircle mc2 = new MyCircle(new MyPoint(2,2),4);
        System.out.println(mc2);
    }
    
    
        
    
}
