

package javabasic.nico_mathew_sia;


public class Test{

    public static void main(String[] args)
    {
        //Movable m1 = new MovablePoint(5,6,10); needs to have 4 paramaters
        Movable m1 = new MovablePoint(1,5,6,10);
        System.out.println(m1);
        m1.moveLeft();
        System.out.println(m1);
        
        Movable m2 = new MovablePoint(2,1,2,20);
        System.out.println(m2);
        m2.moveRight();
        System.out.println(m2);
        
        MovableRectangle mr1 = new MovableRectangle(0,4,6,0,2,2);
        System.out.println(mr1);
        mr1.moveRight();
        System.out.println(mr1);
        mr1.moveLeft();
        System.out.println(mr1);
        mr1.moveUp();
        System.out.println(mr1);
        mr1.moveDown();
        System.out.println(mr1);
        
        
        
        
    }
    
    
    
    
}
    
    
