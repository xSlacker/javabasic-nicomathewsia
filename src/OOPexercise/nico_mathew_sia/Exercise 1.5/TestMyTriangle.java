

package javabasic.nico_mathew_sia;


public class TestMyTriangle{

    public static void main(String[] args)
    {

        MyTriangle m = new MyTriangle(0,0,3,3,3,0);
        
        System.out.println("The Triangle :"+m);
        System.out.println("The perimeter :"+m.getPerimeter());
        System.out.println("Type :");
        m.printType();
        
        
        MyPoint v1 = new MyPoint(0,0);
        MyPoint v2 = new MyPoint(3,3);
        MyPoint v3 = new MyPoint(3,0);
        MyTriangle m2 = new MyTriangle(v1,v2,v3);
        
        System.out.println("The Triangle :"+m2);
        System.out.println("The perimeter :"+m2.getPerimeter());
        System.out.println("Type :");
        m2.printType();
    }
    
    
        
    
}
