
package javabasic.nico_mathew_sia;


public class Customer {
    
    private String name;
    private boolean member;
    private String memberType;
    
    public Customer(String name)
    {
        this.name = name;
        member = false;
        memberType = "";
    }
    
    public String getName()
    {
        return name;
    }
    
    public boolean isMember()
    {
        return member;
    }
    
    public void setMember(boolean member)
    {
        this.member = member;
    }
    
    public String getMemberType()
    {
        return memberType;
    }
    
    public void setMemberType(String memberType)
    {
        this.memberType = memberType;
    }
    
    public String toString()
    {
        if(!isMember())
            return "Customer name is "+name+", not a member"+" and member type is "+memberType;
        
        return "Customer name is "+name+", already a member"+" and member type is "+memberType;
    }
}
