

package javabasic.nico_mathew_sia;

import java.util.Date;


public class Test {

        
    public static void main(String[] args)
    {
        
        Customer c = new Customer("Slacker");
        c.setMember(true);
        c.setMemberType("Premium");
        System.out.println(c);
        
        
        
        Customer c1 = new Customer("Slacker2");
        c1.setMember(false);
        System.out.println(c1);
        
        
        Customer c2 = new Customer("Slacker3");
        c2.setMember(true);
        System.out.println(c2);
        
        Visit v = new Visit(c,new Date(10,10,2014));
        v.setProductExpense(1000);
        v.setServiceExpense(1000);
        System.out.println(v);
        System.out.println(c.getName()+" total expenses : "+v.getTotalExpense());
        if(c.isMember())
        {
            System.out.println(c.getName()+" Discounted expenses : "
                    + getDiscountedExpense(v.getServiceExpense(),v.getProductExpense(),c.getMemberType()));
        }
        else
            System.out.println(c.getName()+" Discounted expenses : "+v.getTotalExpense());
        
    
        
        
        
        Visit v2 = new Visit(c1,new Date(01,12,2014));
        v2.setProductExpense(2000);
        v2.setServiceExpense(2000);
        System.out.println(v2);

        System.out.println(c.getName()+" total expenses : "+v2.getTotalExpense());
        if(c1.isMember())
        {
            System.out.println(c1.getName()+" Discounted expenses : "
                    + getDiscountedExpense(v2.getServiceExpense(),v2.getProductExpense(),c1.getMemberType()));
        }
        else
            System.out.println(c1.getName()+" Discounted expenses : "+v2.getTotalExpense());
        
        
        
        
        Visit v3 = new Visit(c2,new Date(01,12,2014));
        v3.setProductExpense(2000);
        v3.setServiceExpense(2000);
        System.out.println(v3);
        
        
        System.out.println(c2.getName()+" total expenses : "+v3.getTotalExpense());
        if(c2.isMember())
        {
            System.out.println(c2.getName()+" Discounted expenses : "
                    + getDiscountedExpense(v3.getServiceExpense(),v3.getProductExpense(),c2.getMemberType()));
        }
        else
            System.out.println(c2.getName()+" Discounted expenses : "+v3.getTotalExpense());
        
        
        
        
    }
    
    public static double getDiscountedExpense(double productEx,double serviceEx,String memberType)
    {
        return (serviceEx - (DiscountRate.getServiceDiscountRate(memberType)*serviceEx)) 
                + (productEx - (DiscountRate.getProductDiscountRate(memberType)*productEx));
    }
    
    
    
}
    
    
