package javabasic.nico_mathew_sia;
import java.util.Date;

public class Visit {

    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;
    
    
    
    public Visit(Customer customer, Date date)
    {
        this.customer = customer;
        this.date = date;
    }
    
    public String getName()
    {
        return customer.getName();
    }
    
    public double getServiceExpense()
    {
        return this.serviceExpense;
    }
    
    
    public void setServiceExpense(double ex)
    {
        this.serviceExpense = ex;
    }
    
    public double getProductExpense()
    {
        return this.productExpense;
    }
    
    public void setProductExpense(double ex)
    {
        this.productExpense = ex;
    }
    
    public double getTotalExpense()
    {
        return productExpense + serviceExpense;
    }
    
    public String toString()
    {
        return  customer.toString()+"\nDate : "+date.toString()+
                "\nService Expense : "+serviceExpense+"\nProduct Expense : "+productExpense;
    }
    
    
}
