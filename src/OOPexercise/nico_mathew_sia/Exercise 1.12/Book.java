

package javabasic.nico_mathew_sia;

public class Book {

    private String name;
    private Author[] authors = new Author[5];
    private int numAuthors = 0;
    private double price;
    private int qtyInStock;

    public Book(String name,double price)
    {
        this.name = name;
        this.price = price;

    }
    public Book(String name,double price,int qtyInStock)
    {
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;

    }

    public String getName()
    {
        return this.name;
    }

    public Author[] getAuthor()
    {
        return this.authors;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getQtyInstock()
    {
        return this.qtyInStock;
    }

    public void setQtyInstock()
    {
        this.qtyInStock = qtyInStock;
    }

    public String toString()
    {
        String authors = "";
        if(numAuthors>0)
        {
            for(int x=0 ; x<this.numAuthors-1;x++)
            {
                authors +=this.authors[x].getName()+",";
            }

                authors +=this.authors[numAuthors-1].getName();

        }

        return this.name+" by "+authors;
    }

    public void printAuthors()
    {
        if(numAuthors>0)
        {
            for(int x=0 ; x<this.numAuthors-1;x++)
            {
                System.out.print(this.authors[x].getName()+",");
            }

            System.out.print(this.authors[numAuthors-1].getName());

        }

    }


    public void addAuthor(Author author)
    {
        authors[numAuthors] = author;
        ++numAuthors;
    }
    
    




}
