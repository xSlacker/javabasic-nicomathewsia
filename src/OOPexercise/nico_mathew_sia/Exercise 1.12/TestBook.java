

package javabasic.nico_mathew_sia;
import java.math.BigInteger;


public class TestBook{

    public static void main(String[] args)
    {
     
       Book javaDummy = new Book("Java for Dummy",19.99 , 99);
       System.out.println(javaDummy);
       System.out.println("The authors are: ");
       javaDummy.printAuthors();
       
       javaDummy.addAuthor(new Author("Tan Ah Teck","AhTeck@somewhere.com",'m'));
       javaDummy.addAuthor(new Author("Paul Tan","paul@nowhere.com",'m'));
       System.out.println(javaDummy);
       System.out.print("The authors are: ");
       javaDummy.printAuthors();
 
       

       System.out.println("\n\n");
       BookRemove javaDummyR = new BookRemove("Java for Dummy",19.99 , 99);
       javaDummyR.addAuthor(new Author("Tan Ah Teck","AhTeck@somewhere.com",'m'));
       javaDummyR.addAuthor(new Author("Paul Tan","paul@nowhere.com",'m'));
       javaDummyR.addAuthor(new Author("dummy1","dummy1@somewhere.com",'m'));
       javaDummyR.addAuthor(new Author("dummy2","dummy2@nowhere.com",'m'));
       System.out.println(javaDummyR);
       System.out.print("The authors are: ");
       javaDummyR.printAuthors();
       
       
       System.out.println("\n\n");
        if(javaDummyR.removeAuthorByName("dummy1"));
            System.out.println("Successfully deleted dummy1");
      
       System.out.println(javaDummyR);
       System.out.print("The authors are: ");
       javaDummyR.printAuthors();
    }
    
    
    
}
    
    
