

package javabasic.nico_mathew_sia;
import java.util.ArrayList;

public class BookRemove {

    private String name;
    
    ArrayList<Author> authors = new ArrayList<Author>();
    private int numAuthors = 0;
    private double price;
    private int qtyInStock;

    public BookRemove(String name,double price)
    {
        this.name = name;
        this.price = price;

    }
    public BookRemove(String name,double price,int qtyInStock)
    {
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;

    }

    public String getName()
    {
        return this.name;
    }

    public ArrayList<Author> getAuthor()
    {
        return this.authors;
    }

    public double getPrice()
    {
        return this.price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getQtyInstock()
    {
        return this.qtyInStock;
    }

    public void setQtyInstock()
    {
        this.qtyInStock = qtyInStock;
    }

    public String toString()
    {
        String authors = "";
        
        numAuthors = this.authors.size();
        if(numAuthors>0)
        {
            for(int x=0 ; x<this.numAuthors-1;x++)
            {
                authors +=this.authors.get(x).getName()+",";
            }

                authors +=this.authors.get(numAuthors-1).getName();

        }

        return this.name+" by "+authors;
    }

    public void printAuthors()
    {
        numAuthors = this.authors.size();
        
        if(numAuthors>0)
        {
            for(int x=0 ; x<this.numAuthors-1;x++)
            {
                System.out.print(this.authors.get(x).getName()+",");
            }

            System.out.print(this.authors.get(numAuthors-1).getName());

        }

    }


    public void addAuthor(Author author)
    {
        authors.add(author);
        ++numAuthors;
    }
    
    
    public boolean removeAuthorByName(String authorName)
    {
   
        for(Author a : authors)
        {
            if(a.getName() == authorName)
            {
                authors.remove(a);
                return true;
            }
        }
        
        return false;
    }
    
    




}
