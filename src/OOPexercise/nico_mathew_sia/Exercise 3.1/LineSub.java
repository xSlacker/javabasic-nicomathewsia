
package javabasic.nico_mathew_sia;

public class LineSub extends Point {
    
    Point end;
    
    public LineSub(Point begin, Point end)
    {
        super(begin.getX(),begin.getY());
        this.end = end;
    }
    
    public LineSub(int beginX,int beginY,int endX,int endY)
    {
        super(beginX,beginY);
        end = new Point(endX,endY);
    }
    
    public Point getBegin()
    {
       return new Point(super.getX(),super.getY()); 
    }
    
    public Point getEnd()
    {
       return end; 
    }
    
    public void setBegin(Point begin)
    {
        super.setXY(begin.getX(), begin.getY());
    }
    
    public void setEnd(Point end)
    {
        this.end = end;
    }
    
    public int getBeginX()
    {
        return super.getX();
    }
        
    public int getBeginY()
    {
        return super.getY();
    }
    
    public int getEndX()
    {
        return end.getX();
    }
        
    public int getEndY()
    {
        return end.getY();
    }
    
    public void setBeginX(int beginX)
    {
        super.setX(beginX);
    }
    
    public void setBeginY(int beginY)
    {
        super.setY(beginY);
    }
    
    public void setBeginXY(int beginX,int beginY)
    {
        super.setXY(beginX, beginY);
    }
    
    public void setEndX(int endX)
    {
        end.setX(endX);
    }
    
    public void setEndY(int endY)
    {
        end.setY(endY);
    }
    
    public void setEndXY(int endX,int endY)
    {
        end.setXY(endX, endY);
    }
    
    public int getLength()
    {
        return (int)Math.sqrt( ( ( super.getX() - end.getX() ) * ( super.getX() - end.getX() ) ) +
                ( ( super.getY() - end.getY() ) * ( super.getY() - end.getY() ) ) );
    }
    
    public int getGradient()
    {
        return (int) Math.atan2(( super.getX() - end.getX() ), ( super.getY() - end.getY() ));
    }
    
    public String toString()
    {
        return "A line with a Begin "+super.toString()+" and an End "+end.toString();
    }
     
    
}
