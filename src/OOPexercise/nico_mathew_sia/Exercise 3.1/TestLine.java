

package javabasic.nico_mathew_sia;

public class TestLine {
 
    public static void main(String[] args)
    {

        Line l1 = new Line(0,0,3,4);
        System.out.println(l1);
        System.out.println("Length : "+l1.getLength());
        
        Point p1 = new Point( 2,4);
        Point p2 = new Point( 9,4);
        Line l2 = new Line(p1,p2);
        System.out.println(l2);
        System.out.println("Length : "+l2.getLength());
        
 
        System.out.println("\n****************LINE SUB***************");        
        LineSub lb1 = new LineSub(0,0,3,4);
        System.out.println(lb1);
        System.out.println("Length : "+lb1.getLength());
        
        Point p3 = new Point( 2,4);
        Point p4 = new Point( 9,4);
        LineSub lb2 = new LineSub(p3,p4);
        System.out.println(lb2);
        System.out.println("Length : "+lb2.getLength());
        
        
    }
    
}
