package javabasic.nico_mathew_sia;

public class MyIntStackMod2 {

    private int[] contents;
    private int tos;
    
    public MyIntStackMod2(int capacity)
    {
        contents = new int[capacity];
        tos = -1;
    }
    
    public boolean push(int element)
    {
        try
        {
            contents[++tos] = element;
        }   
        catch(Exception c)
        {
            return false;
        }
        
        return true;
    }
    
    public int pop()
    {
        return contents[tos--];
    }
    
    public int peek()
    {
        return contents[tos];
    }
    
    public boolean isEmpty()
    {
        return tos < 0;
    }
    
    public boolean isFull()
    {
        return tos == contents.length -1;
    }
}
