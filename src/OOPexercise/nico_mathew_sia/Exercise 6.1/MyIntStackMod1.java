
package javabasic.nico_mathew_sia;

public class MyIntStackMod1 {
    
    private int[] contents;
    private int tos;
    
    public MyIntStackMod1(int capacity)
    {
        contents = new int[capacity];
        tos = -1;
    }
    
    public void push(int element)
    {
        if(isFull())
        {
            throw new IllegalStateException("Stack is Full!");
        }
        else
            contents[++tos] = element;
    }
    
    public int pop()
    {
        return contents[tos--];
    }
    
    public int peek()
    {
        return contents[tos];
    }
    
    public boolean isEmpty()
    {
        return tos < 0;
    }
    
    public boolean isFull()
    {
        return tos == contents.length -1;
    }    
}
