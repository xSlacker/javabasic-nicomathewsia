package javabasic.nico_mathew_sia;


public class MyIntStackMod3 {

    private int[] contents;
    private int tos;
    
    public MyIntStackMod3(int capacity)
    {
        contents = new int[capacity];
        tos = -1;
    }
    
    public void push(int element)
    {
        if(isFull())
        {
            int[] temp = new int[contents.length+1];
            for(int i=0; i<=contents.length-1;i++)
            {
                temp[i] = contents[i];
            }
            
            contents = temp;
            contents[tos++] = element;
            
        }
        else
            contents[++tos] = element;
        
    }
    
    public int pop()
    {
        return contents[tos--];
    }
    
    public int peek()
    {
        return contents[tos];
    }
    
    public boolean isEmpty()
    {
        return tos < 0;
    }
    
    public boolean isFull()
    {
        return tos == contents.length -1;
    }
}
