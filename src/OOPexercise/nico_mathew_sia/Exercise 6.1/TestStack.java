

package javabasic.nico_mathew_sia;

import java.util.Date;


public class TestStack {

        
    public static void main(String[] args)
    {
        
        System.out.println("\n********Normal**********");
        
        MyIntStack m1 = new MyIntStack(5);
        System.out.println("Pushing...");
        m1.push(1);
        checkStatus(m1);
        System.out.println("Pushing...");
        m1.push(1);
        checkStatus(m1);
        System.out.println("Pushing...");
        m1.push(1);
        checkStatus(m1);
        System.out.println("Pushing...");
        m1.push(1);
        checkStatus(m1);
        System.out.println("Pushing...");
        m1.push(1);
        checkStatus(m1);
        
        
        System.out.println("\n********Modify 2**********");
        MyIntStackMod2 m3 = new MyIntStackMod2(1);
        System.out.println("Pushing...");
        m3.push(1);
        checkStatus(m3);
        System.out.println("Pushing...");
        if(m3.push(1))
            checkStatus(m3);
        else
            System.out.println("Unsuccessfull");
        
        
        System.out.println("\n********Modify 3**********");
        MyIntStackMod3 m4 = new MyIntStackMod3(2);
        System.out.println("Pushing ...");
        m4.push(1);
        System.out.println("Pushing Should be full by now...");
        m4.push(1);
        System.out.println("Pushing still... Reallocate if full.");
        m4.push(1);
        checkStatus(m3);
        
        
        
        
        System.out.println("\n********Modify 1**********");
        MyIntStackMod1 m2 = new MyIntStackMod1(4);
        System.out.println("Pushing...");
        m2.push(1);
        checkStatus(m2);
        System.out.println("Pushing...");
        m2.push(1);
        checkStatus(m2);
        System.out.println("Pushing...");
        m2.push(1);
        checkStatus(m2);
        System.out.println("Pushing...");
        m2.push(1);
        checkStatus(m2);
        System.out.println("Pushing... Should be illegal.."
                + "");
        m2.push(1);
        checkStatus(m2);
        
        
     
        
            
    
       
    }
    
    public static void checkStatus(MyIntStack m)
    {
        if(m.isFull())
            System.out.println("Already Full");
        else
            System.out.println("Not full");
        
    }
    
    
    public static void checkStatus(MyIntStackMod1 m)
    {
        if(m.isFull())
            System.out.println("Already Full");
        else
            System.out.println("Not full");
        
    }
    
    
    public static void checkStatus(MyIntStackMod2 m)
    {
        if(m.isFull())
            System.out.println("Already Full");
        else
            System.out.println("Not full");
        
    }
    
    
    public static void checkStatus(MyIntStackMod3 m)
    {
        if(m.isFull())
            System.out.println("Already Full");
        else
            System.out.println("Not full");
        
    }
    
    
    
    
}
    
    
