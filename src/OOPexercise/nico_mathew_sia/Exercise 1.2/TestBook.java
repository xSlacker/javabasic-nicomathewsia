

package javabasic.nico_mathew_sia;


public class TestBook{

    public static void main(String[] args)
    {
        
        Author anAuthor = new Author("Tan Ah Teck","ahteck@somewhere.com",'m');
        Book aBook = new Book("Java for dummy", anAuthor,19.95,1000);
        Book anotherBook = new Book("Java for dummy New Book", 
                    new Author("Someone","someone@somewhere.com",'m'),20.99,200);
   
        System.out.println(" aBook Author name: "+aBook.getAuthor().getName());
        System.out.println(" aBook Author name: "+aBook.getAuthor().getEmail());
        System.out.println(" aBook Author name: "+aBook.getAuthorName());
        System.out.println(" aBook Author name: "+aBook.getAuthorEmail());
        
        
        
        
    }
    
    
        
    
}
