

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class Hex2Dec {


    public static void main(String[] args)
    {
        String hexStr;
        int dec=0;
        char hexChar;

        Scanner in = new Scanner(System.in);
        System.out.print("Enter a hexadecimal string : ");
        hexStr = in.nextLine();

        for(int i=0;i<hexStr.length();i++)
        {
            hexChar = hexStr.charAt(i);

            if(hexChar >= '0' && hexChar <= '9')
            {
                dec += (hexChar-'0') * Math.pow(16, ((hexStr.length()-1)-i));
            }
            else if (hexChar >= 'a' && hexChar <= 'f')
            {
                dec += ((hexChar-'a') + 10) * Math.pow(16, ((hexStr.length()-1)-i));
            }
            else if (hexChar >= 'A' && hexChar <= 'F')
            {
                dec += ((hexChar-'A') + 10) * Math.pow(16, ((hexStr.length()-1)-i));
            }
            else
            {
                System.out.println("Error: Invalid Hexadecimal String");
                System.exit(0);
            }
        }

        System.out.println("The equivalent decimal number for hexadecimal \""+hexStr+"\" is "+dec);


    }


}
