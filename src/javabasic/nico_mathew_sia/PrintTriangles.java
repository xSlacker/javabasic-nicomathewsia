

package javabasic.nico_mathew_sia;
import java.util.Scanner;


public class PrintTriangles{

    public static int size;
 
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of rows : ");
        size = in.nextInt();
     

 
         printPowerOf2Triangle();
         printPascalTriangle1();
         printPascalTriangle2();
    }
    
    public static void printPascalTriangle1()
    {
        for(int i=0;i<size;i++)
        {
               int num=1;
               System.out.format("%"+(size-i)*2+"s"," ");
               for(int j=0;j<=i;j++)
                 {
                   System.out.format("%4d",num);
                   num=num*(i-j)/(j+1);
                 }
             System.out.println();
            }
    }
    
    public static void printPowerOf2Triangle()
    {
        int val=1;
        for (int i=0; i<size; i++)
        {
            for (int k=0; k<size-i; k++)
            {
                System.out.print("    ");
            }
            
            for (int j=0; j<=i; j++)
            {
                if(j==0)
                    val=1;
                else
                    val =val*2;
                
                if(val<10)
                    System.out.print(""+(val)+"   ");
                else if(val<100)
                    System.out.print(""+(val)+"  ");  
                else                   
                    System.out.print(""+(val)+" ");  
            }
            for (int j=i; j>0; j--)
            {
                val =val/2;
                
                if(val<10)
                    System.out.print(""+(val)+"   ");
                else if(val<100)
                    System.out.print(""+(val)+"  ");  
                else                   
                    System.out.print(""+(val)+" ");  
            }

            System.out.println();
        }

    }
    
    public static void printPascalTriangle2() 
    {        
        int num;
        
        for(int i=0; i<=size; i++) 
        {           
            num = 1;            
            for(int j=0; j<=i; j++) 
            {
                if(j>0) 
                {
                    num = num * ((i+1) - j) / j;                    
                }                
                System.out.print(num + " ");                    
            }
            System.out.println();
    }
}
    
        
    
}
