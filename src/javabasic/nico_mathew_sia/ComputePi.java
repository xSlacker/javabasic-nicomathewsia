

package javabasic.nico_mathew_sia;

public class ComputePi {


    public static void main(String[] args) {
        
       double sum = 0;
       int maxDenom = 10000000;
       
       for(int denom = 1; denom <= maxDenom ; denom = denom + 2)
       {
           if(denom % 4 == 1)
           {
               sum += (double)1/(double)denom;
           }
           else if(denom % 4 == 3)
           {
               sum -= (double)1/(double)denom;
           }
           else
           {
               System.out.println("The computer has gone crazy?!");
           }
           
       }
      
    }
    
    
   
    
}
