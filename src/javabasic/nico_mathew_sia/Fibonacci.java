

package javabasic.nico_mathew_sia;

public class Fibonacci {


    public static void main(String[] args) {
     
       int n=1;
       int fn;
       int nMax = 20;
       int sum = 0;
       int count = 0;
       double average;
       
       System.out.println("The first "+ nMax + " Fibonacci numbers are:");
        
       
       while(n <= nMax)
       {
           System.out.print(""+fib(n)+" ");
           sum += fib(n);
           n++;
           count++;
       }
       System.out.println("\nThe average is "+((double)sum/(double)count));
    }
    
    
     public static int fib(int n) 
     {
        if (n == 1 || n == 2)
            return 1;
        else 
            return fib(n-1)+ fib(n-2);
     }
    
   
    
}
