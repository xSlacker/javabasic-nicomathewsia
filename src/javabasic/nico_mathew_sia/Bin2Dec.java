

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class Bin2Dec {


    public static void main(String[] args)
    {
        String inStr;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a binary string : ");
        inStr = in.next();
        if(isBinary(inStr) == true)
            System.out.println("The equivalent decimal number for binary \""+inStr+"\" is "+bintoDec(inStr));
        else
            System.out.println("Error : Invalid Binary String \""+inStr+"\"");
        
        
    }
    
    public static boolean isBinary(String binary)
    {
        for(int b=0; b<binary.length() ; b++)
        {
            if((binary.charAt(b) != '1') && (binary.charAt(b)!= '0') )
                return false;
        }
        return true;
    }
        
    public static int bintoDec(String binary)
    {
        int bin = Integer.parseInt(binary);
        int decimal = 0;
        int power = 0;
        while(true)
        {
            if(bin == 0)
                break;
            else 
            {
                int tmp = bin%10;
                decimal += tmp*Math.pow(2, power);
                bin = bin/10;
                power++;
            }
        }
        return decimal;
    }
    

   
    
}
