

package javabasic.nico_mathew_sia;

public class SumAndAverage {


    public static void main(String[] args) {
        
        int sum = 0;
        double average;
        int lowerbound = 1;
        int upperbound = 100;
        
        for(int number = lowerbound; number <= upperbound; number++)
        {
            sum += number;
        }
        
        average = (double)sum / (Math.abs(upperbound - lowerbound) + 1) ;
        System.out.println("Sum is : "+sum);
        System.out.println("Average is : "+average+"\n");
        
        
        modification1(lowerbound,upperbound); //call modification 1
        modification2(lowerbound,upperbound); //call modification 2
        modification4();                      //call modification 4
        modification5();                      //call modification 5
        modification6();                      //call modification 6
        modification7();                      //call modification 7
        
        
        
        
        
        
    }
    
    
    /******************** Modification 1 *********************/   
    public static void modification1(int lb ,int ub)
    {
        int sum = 0;
        double average;
        int lowerbound = lb;
        int upperbound = ub;
        int number = lowerbound;
        
        while(number <= upperbound)
        {
            sum += number;
            number++;
        }
        
        average = (double)sum / (Math.abs(upperbound - lowerbound) + 1) ;
        System.out.println("Modification 1");
        System.out.println("Sum is : "+sum);
        System.out.println("Average is : "+average+"\n");
    }
    
 
    /******************** Modification 2 *********************/   
    public static void modification2(int lb ,int ub)
    {
        int sum = 0;
        double average;
        int lowerbound = lb;
        int upperbound = ub;
        int number = lowerbound;
        
        do
        {
            sum += number;
            number++;
        }while(number <= upperbound);
        
        average = (double)sum / (Math.abs(upperbound - lowerbound) + 1) ;
        System.out.println("Modification 2");
        System.out.println("Sum is : "+sum);
        System.out.println("Average is : "+average+"\n");
    }
    
    
    /******************** Modification 4 *********************/   
    public static void modification4()
    {
        int sum = 0;
        double average;
        int lowerbound = 111;
        int upperbound = 8899;
        int count = 0;
        
        for(int number = lowerbound; number <= upperbound; number++)
        {
            count++;
        }
        System.out.println("Modification 4");
        System.out.println("Count is : "+count+"\n");
    }
    
    
    /******************** Modification 5 *********************/   
    public static void modification5()
    {
        int sum = 0;
        double average;
        int lowerbound = 1;
        int upperbound = 100;
        int count = 0;
        
        for(int number = lowerbound; number <= upperbound; number++)
        {
            if(number%2==1)
            {
                sum+=number;
                count++;
            }
        }
        
        average = (double)sum / (double)count ;
        System.out.println("Modification 5");
        System.out.println("Sum is : "+sum);
        System.out.println("Average is : "+average+"\n");
    }
   
    
    /******************** Modification 6 *********************/   
    public static void modification6()
    {
        int sum = 0;
        double average;
        int lowerbound = 1;
        int upperbound = 100;
        int count = 0;
        
        for(int number = lowerbound; number <= upperbound; number++)
        {
            if(number%7==1)
            {
                sum+=number;
                count++;
            }
        }
        
        average = (double)sum / (double)count ;
        System.out.println("Modification 6");
        System.out.println("Sum is : "+sum);
        System.out.println("Average is : "+average+"\n");
    }
    
    
    /******************** Modification 7 *********************/   
    public static void modification7()
    {
        int sum = 0;
        double average;
        int lowerbound = 1;
        int upperbound = 100;
        
        for(int number = lowerbound; number <= upperbound; number++)
        {
            sum+= (number*number);
        }
        System.out.println("Modification 7");
        System.out.println("Sum of the squares is : "+sum+"\n");
    }
    
    
}
