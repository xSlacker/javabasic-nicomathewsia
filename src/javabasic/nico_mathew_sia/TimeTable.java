

package javabasic.nico_mathew_sia;

public class TimeTable {


    public static void main(String[] args) {
     
        int size = 12;

        
        System.out.print("* | ");
        
        for (int x = 1; x <= 2; x++) 
        {
            for (int h = 1; h <= size; h++) 
            {
                if(x==1)
                {
                    if((h/100)>=1 )
                        System.out.print(h + " ");
                    else if((h/10)>=1 )
                        System.out.print(" "+h + " ");
                    else
                        System.out.print(" "+h + "  ");
                    
                }
                else
                {
                    if(h==size)
                    {
                        System.out.print("---");
                    }
                    System.out.print("----");
                }
            }
            System.out.println();
            
        }
        
        for (int i = 1; i <= size; i++) 
        {
            if((i/10)>=1 )
                System.out.print(i + "| ");
            else
                System.out.print(i + " | ");
            for (int j = 1; j <= size; j++) 
            {
                if(((i*j)/100)>=1 )
                    System.out.print(i*j + " ");
                else if(((i*j)/10)>=1 )
                    System.out.print(" "+i*j + " ");
                else
                    System.out.print(" "+i*j + "  ");
            }
         System.out.println();
        }
    }
    

   
    
}
