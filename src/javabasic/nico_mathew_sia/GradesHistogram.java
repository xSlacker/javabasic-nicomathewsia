

package javabasic.nico_mathew_sia;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class GradesHistogram {

    public static int[] grades;
    public static int[] bins = new int[10];
    
    public static void main(String[] args) throws FileNotFoundException 
    {
        readGrades("grades.in");
        computeHistogram();
        printHistogramHorizontal();
        printHistogramVertical();
    }
   
    
    public static void readGrades(String filename) throws FileNotFoundException 
    {
        int numStud;
        Scanner in = new Scanner(new File(filename));
        numStud = in.nextInt();
        grades = new int[numStud];
        
        for(int i=0; i <grades.length;i++)
        {
            grades[i]=in.nextInt();
        }
        

    }
    public static void computeHistogram()
    {
        
        for(int i=0; i <grades.length;i++)
        {
            if(grades[i]>=0 && grades[i]<=9 )
                bins[0]++;
            else if(grades[i]>=10 && grades[i]<=19 )
                bins[1]++;
            else if(grades[i]>=20 && grades[i]<=29 )
                bins[2]++;
            else if(grades[i]>=30 && grades[i]<=39 )
                bins[3]++;
            else if(grades[i]>=40 && grades[i]<=49 )
                bins[4]++;
            else if(grades[i]>=50 && grades[i]<=59 )
                bins[5]++;
            else if(grades[i]>=60 && grades[i]<=69 )
                bins[6]++;
            else if(grades[i]>=70 && grades[i]<=79 )
                bins[7]++;
            else if(grades[i]>=80 && grades[i]<=89 )
                bins[8]++;
            else if(grades[i]>=90 && grades[i]<=100 )
                bins[9]++;
            
        }
    }
    
    public static void printHistogramHorizontal()
    {
        for(int i=0;i<10;i++)
        {
            switch(i)
            {
                case 0 : System.out.print("0 -   9:  "); break;
                case 1 : System.out.print("1 -  19:  "); break;
                case 2 : System.out.print("2 -  29:  "); break;
                case 3 : System.out.print("3 -  39:  "); break;
                case 4 : System.out.print("4 -  49:  "); break;
                case 5 : System.out.print("5 -  59:  "); break;
                case 6 : System.out.print("6 -  69:  "); break;
                case 7 : System.out.print("7 -  79:  "); break;
                case 8 : System.out.print("8 -  89:  "); break;
                case 9 : System.out.print("9 - 100:  "); break;
            }
            
            for(int j=1;j<=bins[i];j++)
            {
                System.out.print("*");
            }
            
            System.out.println();
        }
    }
    
    public static void printHistogramVertical()
    {
        int maxNum=0;
        for(int m=0;m<bins.length;m++)
        {
            if(bins[m]>=maxNum)
                maxNum = bins[m];
        }
        
        
        for(int i=maxNum;i>=1;i--)
        {
            for(int j=0;j<bins.length;j++)
            {
                if(bins[j]>=i)
                    System.out.print("   *   ");
                else
                    System.out.print("       ");
            }
            
            System.out.println();
        }
        System.out.println("  0-9   10-19  20-29  30-39  40-49  50-59  60-69  70-79  80-89  90-100");
        
        
    }
    
}
