

package javabasic.nico_mathew_sia;

public class HarmonicSum {


    public static void main(String[] args) {
        
        int maxDenominator = 50000;
        double difference = 0.0;
        double sumL2R = 0.0;
        double sumR2L = 0.0;
        
        for(int denominator = 1; denominator <= maxDenominator; denominator++)
        {
            sumL2R += ((double)1/(double)denominator); 
        }
        
        System.out.println("Sum left to right : "+sumL2R);
        
        for(int max = maxDenominator; max >= 1; max--)
        {
            sumR2L += ((double)1/(double)max); 
        }
        
        System.out.println("Sum right to left : "+sumR2L);
        difference = sumL2R - sumR2L;
        System.out.println("Differenece : : "+difference);
        
    }
    
    
   
    
}
