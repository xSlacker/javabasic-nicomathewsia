

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class GradesStatistics {

    public static int[] grades;
    public static double sumOfGrades = 0;

    public static void main(String[] args)
    {
        readGrades();
        System.out.println("The average is " + average());
        System.out.println("The minimum is " + min());
        System.out.println("The maximum is " + max());
        System.out.println("The standard deviation is " + stdDev() );
    }
   
    
    public static void readGrades()
    {
        int numStud = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        numStud = in.nextInt();
        grades = new int[numStud];
        
        boolean isValid;
        for(int i=0; i <grades.length;i++)
        {
            isValid = false;
            while(!isValid)
            {
                System.out.print("Enter the grade for student "+(i+1)+": ");
                grades[i]=in.nextInt();
                if(grades[i]>=0 && grades[i]<=100)
                    isValid = true;
                else
                    System.out.println("Invalid grade, try again...");
            }
            sumOfGrades += grades[i];
            
        }
        

    }
    public static double average()
    {
        return (sumOfGrades / (double)grades.length);
    }
    
    public static int max()
    {
        int maxValue=0;
        for(int i=0; i <grades.length;i++)
        {
            if(grades[i] > maxValue)
                maxValue = grades[i];
        }
        return maxValue;
    }
    public static int min()
    {
        
        int minValue=0;
        for(int i=0; i <grades.length;i++)
        {
            if(grades[i] < minValue)
                minValue = grades[i];
        }
        return minValue;
    }
    public static double stdDev()
    {
        double sum = 0.0;
        for(int a : grades)
	{
            sum += a;
	}
        double mean = sum/grades.length;
        double temp = 0;
        for(int a :grades)
	{
       	    temp += (mean-a)*(mean-a);
	}
	return Math.sqrt(temp/grades.length);   
    }
    
    
    
}
