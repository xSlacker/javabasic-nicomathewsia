

package javabasic.nico_mathew_sia;


public class PrintPattern {

    public static void main(String[] args)
    {
       printPatternA(8);
       printPatternB(8);
       printPatternC(8);
       printPatternD(8);


       printPatternJ(8);
       printPatternK(8);
       printPatternL(6);
       printPatternM(8);
       printPatternN(8);
       printPatternO(8);
       printPatternP(8);
       printPatternQ(8);
       printPatternR(8);
       printPatternS(8);
       printPatternT(8);
       printPatternU(8);


       printPatternE(7);

       printPatternF(7);

       printPatternG(7);

       printPatternH(7);

       printPatternI(7);

    }

    public static void printPatternA(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= line; star++ )
            {

                System.out.print( "# " );
            }
            System.out.println();
        }

        System.out.println("(a)\n");

    }

    public static void printPatternB(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= size; star++ )
            {

                if(line<=star)
                    System.out.print( "# " );
            }
            System.out.println();
        }


        System.out.println("(b)\n");

    }

    public static void printPatternC(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= size; star++ )
            {

                if(line<=star)
                    System.out.print( "# " );
                else
                  System.out.print("  ");
            }
            System.out.println();
        }


        System.out.println("(c)\n");

    }

    public static void printPatternD(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= size; star++ )
            {

                if(line>size-star)
                    System.out.print( "# " );
                else
                    System.out.print("  ");
            }
            System.out.println();
        }


        System.out.println("(d)\n");

    }

    public static void printPatternM(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= line; star++ )
            {

                System.out.print(""+star+" ");
            }
            System.out.println();
        }

        System.out.println("(m)\n");

    }

    public static void printPatternN(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = 1; star <= size; star++ )
            {
                if(line<=star)
                    System.out.print( ""+(star-line+1)+" " );
                else
                  System.out.print("  ");
            }
            System.out.println();
        }
        System.out.println("(n)\n");
    }


    public static void printPatternO(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = size; star >= 1; star-- )
            {

                if(line>=star)
                    System.out.print( ""+star +" ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }


        System.out.println("(o)\n");

    }

    public static void printPatternP(int size)
    {
        for(int line = 1; line <= size; line++ )
        {
            for(int star = size; star >= 1; star-- )
            {

                if(line<=star)
                    System.out.print( ""+(star-line+1)+" " );
            }
            System.out.println();
        }


        System.out.println("(p)\n");

    }


    public static void printPatternJ(int size)
    {
        for(int line=1;line<=size;line++)
        {
            for(int s=line;s>1;s--)
            {
                    System.out.print("  ");
            }
            for(int star=1;star<=size-(line-1);star++)
            {
                System.out.print("# ");
                for(int star1=1;star1<star;star1+=star)
                {
                    System.out.print("# ");
                }
            }
                System.out.println();
        }

        System.out.println("(j)\n");

    }


    public static void printPatternK(int size)
    {
        for(int line=1;line<=size;line++)
        {
            for(int s=1;s<size-(line-1);s++)
            {
                System.out.print("  ");
            }
            for(int star=1;star<=line;star++)
            {
                System.out.print("# ");
                for(int star1=1;star1<star;star1+=star)
                {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }

        System.out.println("(k)\n");
    }

    public static void printPatternL(int size)
    {

        for(int line=1;line<=size;line++)
        {
            for(int s=1;s<size-(line-1);s++)
            {
                System.out.print("  ");
            }
            for(int star=1;star<=line;star++)
            {
                System.out.print("# ");
                for(int star1=1;star1<star;star1+=star)
                {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
        for(int line=2;line<=size;line++)
        {
            for(int s=line;s>1;s--)
            {
                    System.out.print("  ");
            }
            for(int star=1;star<=size-(line-1);star++)
            {
                System.out.print("# ");
                for(int star1=1;star1<star;star1+=star)
                {
                    System.out.print("# ");
                }
            }
                System.out.println();
        }

        System.out.println("(l)\n");
    }


    public static void printPatternQ(int size)
    {
        for (int i=0; i<size; i++)
        {
            for (int k=0; k<size-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<=i; j++)
            {
                System.out.print(""+(j+1)+" ");
            }
            for (int j=i; j>0; j--)
            {
                System.out.print(""+j+" ");
            }

            System.out.println();
        }

            System.out.println("(q)\n");
    }

    public static void printPatternR(int size)
    {
        for (int i=size; i>0; i--)
        {
            for (int k=size-i; k>0; k--)
            {
                System.out.print("  " );
            }
            for (int j=0; j<i; j++)
            {
                System.out.print(""+(j+1)+" ");
            }
            for (int j=i-1; j>0; j--)
            {
                System.out.print(""+j+" ");
            }

            System.out.println();
        }

            System.out.println("(r)\n");
    }

    public static void printPatternS(int size)
    {
        for(int x=1;x<=size;x++)
        {
            for(int y=1;y<=x;y++)
            {
                System.out.print(""+y+" ");
            }
            for(int s=1;s<((size-x)*2);s++)
            {

                    System.out.print("  ");
            }
            for(int y=x;y>=1;y--)
            {
                if(!(x==size && y==x))
                    System.out.print(""+y+" ");
            }
            System.out.println();
        }
            System.out.println("(s)\n");
    }
    public static void printPatternT(int size)
    {
        for(int x=size;x>=1;x--)
        {
            for(int y=1;y<=x;y++)
            {
                System.out.print(""+y+" ");
            }

            for(int s=1;s<((size-x)*2);s++)
            {

                    System.out.print("  ");
            }
            for(int y=x;y>=1;y--)
            {
                if(!(x==size && y==x))
                    System.out.print(""+y+" ");
            }
            System.out.println();
        }
            System.out.println("(t)\n");
    }

    public static void printPatternU(int size)
    {
        for (int i=0; i<size; i++)
        {
            for (int k=0; k<size-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<=i; j++)
            {
                int value = (j+i+1);
                if(value<10)
                    System.out.print(""+value+" ");
                else
                    System.out.print(""+(value%10)+" ");
            }
            for (int j=i; j>0; j--)
            {
                int value = (j+i);
                if(value<10)
                    System.out.print(""+value+" ");
                else
                    System.out.print(""+(value%10)+" ");
            }

            System.out.println("");
        }

            System.out.println("(u)\n");

    }

    public static void printPatternE(int size)
    {
        for (int i=1; i<=size; i++)
        {
            if(i==1 || i==size)
            {
                for (int j=1; j<=size; j++)
                {
                    System.out.print("# ");
                }
            }
            else
            {
                System.out.print("#");
                for (int k=1; k<size-1; k++)
                {
                    System.out.print("  ");
                }
                System.out.print(" #");
            }
            System.out.println();
        }

            System.out.println("(e)\n");

    }

    public static void printPatternF(int size)
    {
        for (int i=1; i<=size; i++)
        {
            if(i==1 || i==size)
            {
                for (int j=1; j<=size; j++)
                {
                    System.out.print("# ");
                }
            }
            else
            {
                for (int k=1; k<i; k++)
                {
                    System.out.print("  ");
                }
                System.out.print("# ");
            }
            System.out.println();
        }

            System.out.println("(f)\n");

    }

    public static void printPatternG(int size)
    {
        for (int i=1; i<=size; i++)
        {
            if(i==1 || i==size)
            {
                for (int j=1; j<=size; j++)
                {
                    System.out.print("# ");
                }
            }
            else
            {
                for (int k=i; k<size; k++)
                {
                    System.out.print("  ");
                }
                System.out.print("# ");
            }
            System.out.println();
        }

            System.out.println("(g)\n");

    }


    public static void printPatternH(int size)
    {


        for (int j=1; j<=size; j++)
        {
            System.out.print("# ");
        }

        System.out.println();
         for (int i=size/2-1; i>0 ;i--)
        {
            for (int k=0; k<size/2-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<i*2+1; j++)
            {
                if(j==0 || j==i*2)
                    System.out.print("# ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }


        for (int i=0; i<size/2; i++)
        {
            for (int k=0; k<size/2-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<i*2+1; j++)
            {
                if(j==0 || j==i*2)
                    System.out.print("# ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
        for (int j=1; j<=size; j++)
        {
            System.out.print("# ");
        }



        System.out.println("\n(h)\n");

    }
    public static void printPatternI(int size)
    {


        for (int j=1; j<=size; j++)
        {
            System.out.print("# ");
        }

        System.out.println();
        int ctr=1;
        for (int i=size/2-1; i>0 ;i--)
        {
            System.out.print("# ");
            for (int k=1; k<size/2-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<i*2+1; j++)
            {
                if(j==0)
                {
                    System.out.print("# ");
                }
                else if(j==i*2)
                {
                    System.out.print("# ");
                    for(int s=1;s<ctr;s++)
                        System.out.print("  ");
                    System.out.print("# ");
                }
                else
                    System.out.print("  ");
            }
            System.out.println();
            ctr++;
        }



        for (int i=0; i<size/2; i++)
        {

            System.out.print("# ");
            for (int k=1; k<size/2-i; k++)
            {
                System.out.print("  ");
            }
            for (int j=0; j<i*2+1; j++)
            {
                if(j==0)
                {
                    System.out.print("# ");
                    if(i==0)
                    {
                        for(int s=1;s<(size/2-i);s++)
                            System.out.print("  ");
                        System.out.print("# ");
                    }
                }
                else if(j==i*2)
                {
                    System.out.print("# ");
                    for(int s=1;s<(size/2-i);s++)
                        System.out.print("  ");
                    System.out.print("# ");
                }
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
        for (int j=1; j<=size; j++)
        {
            System.out.print("# ");
        }



        System.out.println("\n(h)\n");

    }



}
