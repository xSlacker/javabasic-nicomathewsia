

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class TestPalindromicWord {


    public static void main(String[] args)
    {
        int tempLen = 0;
        String inStr;
        String inputString;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a String: ");
        inStr = in.nextLine().toUpperCase();
        inputString = inStr.replaceAll("\\s+", "");
        inputString = inputString.toLowerCase();
        int len = inputString.length()-1;
        System.out.println(inputString);
        for(int i=0;i <= len;i++)
        {

            if (inputString.charAt(i) == inputString.charAt(len-i))
            {
                tempLen += 1;
            }
        }   

        if (tempLen == (len+1))
        {
            System.out.println(inStr+" is a palindrome");
        }
        else
        {
            System.out.println(inStr+" is not a palindrome");
        }
        
    }
    

   
    
}
