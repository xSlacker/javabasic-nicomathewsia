

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class GradesAverage {


    public static void main(String[] args)
    {
        int numStud = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        numStud = in.nextInt();
        double[] grades = new double[numStud];
        double sumOfGrades = 0.0;
        double average = 0.0;
        
        boolean isValid=true;
        for(int i=0; i <grades.length;i++)
        {
            isValid = false;
            while(!isValid)
            {
                System.out.print("Enter the grade for student "+(i+1)+": ");
                grades[i]=in.nextDouble();
                if(grades[i]>=0 && grades[i]<=100)
                    isValid = true;
                else
                    System.out.println("Invalid grade, try again...");
            }
            sumOfGrades += grades[i];
            
        }
        
        average = sumOfGrades / (double)grades.length;
        System.out.println("The average is "+average);
        
        
        
    }
   
    
}
