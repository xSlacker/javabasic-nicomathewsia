

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class Hex2Bin {


    public static void main(String[] args)
    {
        String hexStr;
        String[] hexBits = {"0000","0001","0010","0011",
                            "0100","0101","0110","0111",
                            "1000","1001","1010","1011",
                            "1100","1101","1110","1111",};
        char hexChar;
        String binStr = "";
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a hexadecimal string : ");
        hexStr = in.nextLine();
        
        for(int i=0;i<hexStr.length();i++)
        {
            hexChar = hexStr.charAt(i);
            
            if(hexChar >= '0' && hexChar <= '9')
            {
                binStr += " "+hexBits[hexChar-'0'];
            }
            else if (hexChar >= 'a' && hexChar <= 'f')
            {
                binStr += " "+hexBits[(hexChar-'a')+10];
            }
            else if (hexChar >= 'A' && hexChar <= 'F')
            {
                binStr += " "+hexBits[(hexChar-'A')+10];
            }
            else
            {
                System.out.println("Error: Invalid Hexadecimal String");
                System.exit(0);
            }
        }
        
        System.out.println("The equivalent binary number for hexadecimal \""+hexStr+"\" is "+binStr);
        
        
        
    }
   
    
}
