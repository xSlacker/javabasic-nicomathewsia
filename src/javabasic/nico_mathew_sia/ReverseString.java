

package javabasic.nico_mathew_sia;
import java.util.Scanner;

public class ReverseString {


    public static void main(String[] args)
    {
        String inStr;
        String revString = "";
        int inStrLen;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a String: ");
        inStr = in.next();
        inStrLen = inStr.length();
        for(int s=inStrLen-1; s>=0; s--)
            revString += inStr.charAt(s);
        System.out.print("The reverse of String \""+inStr+"\" is \""+revString+"\"");
    }
    

   
    
}
