

package javabasic.nico_mathew_sia;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class FileScanner {


    public static void main(String[] args) throws FileNotFoundException 
    {
     
        int num1;
        double num2;
        String name;
        double sum;
        
        Scanner in = new Scanner(new File("in.txt"));
        num1 = in.nextInt();
        num2 = in.nextDouble();
        name = in.next();
        
        System.out.println("The integer read is "+num1);
        System.out.println("The floating point number read is "+num2);
        System.out.println("The String read is "+name);
    }
    

   
    
}
